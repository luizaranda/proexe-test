<?php

namespace App\Http\Controllers;
use Lcobucci\JWT\Configuration;

class TokenController extends Controller
{

    public static function generateJwtToken(string $login, string $system): string
    {
        $config = Configuration::forUnsecuredSigner();
        $now   = new \DateTimeImmutable();
        return $config->builder()
            // Configures the id (jti claim)
            ->identifiedBy('4f1g23a12aa')
            // Configures the time that the token was issue (iat claim)
            ->issuedAt($now)
            // Configures the time that the token can be used (nbf claim)
            ->canOnlyBeUsedAfter($now->modify('+1 minute'))
            // Configures the expiration time of the token (exp claim)
            ->expiresAt($now->modify('+1 hour'))
            // Configures a new claim, called "uid"
            ->withClaim('uid', 1)
            ->withClaim('login', $login)
            ->withClaim('system', $system)
            // Builds a new token
            ->getToken($config->signer(), $config->signingKey())
            ->toString();
    }
}

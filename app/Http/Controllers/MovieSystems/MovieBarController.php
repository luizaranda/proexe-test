<?php

namespace App\Http\Controllers\MovieSystems;

class MovieBarController extends AbstractMovieController
{
    protected string $service = \External\Bar\Movies\MovieService::class;

    public function filter($titles): array
    {
        $filtered = [];
        foreach ($titles as $title){
            $filtered[] = $title['title'];
        }
        return $filtered;
    }

}

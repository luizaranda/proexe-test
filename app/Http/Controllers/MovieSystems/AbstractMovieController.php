<?php

namespace App\Http\Controllers\MovieSystems;

use App\Http\Controllers\Controller;

class AbstractMovieController extends Controller implements MovieInterface
{
    protected string $service = '';
    protected int $hits = 0;

    /**
     * @throws \Exception
     */
    public function getTitles(): array
    {
        if($this->hits > 5){
            throw new \Exception('Failure');
        }
        $service = new $this->service;
        try{
            $titles = $service->getTitles();
        } catch(\Exception $e){
            $this->hits++;
            return $this->getTitles();
        }

        return isset($titles['titles']) ? $this->filter($titles['titles']) : $this->filter($titles);
    }

    public function filter($titles): array {
        return $titles;
    }
}

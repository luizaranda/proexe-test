<?php

namespace App\Http\Controllers\MovieSystems;

class MovieBazController extends AbstractMovieController
{
    protected string $service = \External\Baz\Movies\MovieService::class;
}

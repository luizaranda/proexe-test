<?php

namespace App\Http\Controllers\MovieSystems;

interface MovieInterface {

    public function getTitles(): array;
    public function filter($titles): array;
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\MovieSystems\MovieBarController;
use App\Http\Controllers\MovieSystems\MovieBazController;
use App\Http\Controllers\MovieSystems\MovieFooController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getTitles(Request $request): JsonResponse
    {

        $controllers = $this->getControllers();
        $titles = [];
        try{
            foreach ($controllers as $controller){
                $titles = array_merge($titles, (new $controller)->getTitles());
            }
        }catch(\Exception $e){
            return response()->json(['status' => 'failure'], 406);
        }

        return response()->json($titles);
    }

    private function getControllers(): array
    {
        return [
            MovieBarController::class,
            MovieBazController::class,
            MovieFooController::class
        ];
    }
}

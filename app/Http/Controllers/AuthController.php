<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $credentials = $request->only('login', 'password');

        return $this->resolveAuthenticatorAndAuthenticate($credentials);
    }

    private function resolveAuthenticatorAndAuthenticate($credentials): JsonResponse {
        $prefix = ucfirst(strtolower(substr($credentials['login'], 0, 3)));
        $controller = sprintf("\App\Http\Controllers\AuthSystems\Auth%sController", $prefix);
        try{
            $authenticator = app($controller);
            return $authenticator->auth($credentials['login'], $credentials['password']);
        } catch(\Exception $ex){
            return response()->json(['status' => 'failure'], 401);
        }
    }
}

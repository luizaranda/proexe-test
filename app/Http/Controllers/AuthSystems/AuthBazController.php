<?php

namespace App\Http\Controllers\AuthSystems;

class AuthBazController extends AbstractAuthController
{

    protected string $service = \External\Baz\Auth\Authenticator::class;
    protected string $method = 'auth';
    protected string $system = 'BAZ';

    public function isAuth($response): bool {
        return ($response instanceof \External\Baz\Auth\Responses\Success);
    }

}

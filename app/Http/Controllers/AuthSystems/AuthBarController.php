<?php

namespace App\Http\Controllers\AuthSystems;

class AuthBarController extends AbstractAuthController
{
    protected string $service = \External\Bar\Auth\LoginService::class;
    protected string $method = 'login';
    protected string $system = "BAR";
}

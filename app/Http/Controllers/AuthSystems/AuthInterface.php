<?php

namespace App\Http\Controllers\AuthSystems;

use Symfony\Component\HttpFoundation\JsonResponse;

interface AuthInterface {

    public function auth(string $login, string $password): JsonResponse;
    public function isAuth($response): bool;
}

<?php

namespace App\Http\Controllers\AuthSystems;

class AuthFooController extends AbstractAuthController
{

    protected string $service = \External\Foo\Auth\AuthWS::class;
    protected string $method = 'authenticate';
    protected string $system = 'FOO';

    public function isAuth($response): bool {
        return is_null($response);
    }

}

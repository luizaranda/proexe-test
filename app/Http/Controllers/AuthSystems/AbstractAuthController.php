<?php

namespace App\Http\Controllers\AuthSystems;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TokenController;
use Symfony\Component\HttpFoundation\JsonResponse;

class AbstractAuthController extends Controller implements AuthInterface
{

    protected string $service;
    protected string $method;
    protected string $system;

    public function auth(string $login, string $password): JsonResponse
    {
        $authenticator = new $this->service;
        $isAuth = $authenticator->{$this->method}($login, $password);

        if($this->isAuth($isAuth)){
            return response()->json(['status' => 'success', 'token' => TokenController::generateJwtToken($login, $this->system)]);
        }

        return response()->json(['status' => 'failure'], 401);
    }

    public function isAuth($response): bool {
        return $response;
    }
}

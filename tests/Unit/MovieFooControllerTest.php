<?php

namespace Tests\Unit;

use Tests\TestCase;

class MovieFooControllerTest extends TestCase
{
    public function testShoudListTitles() {
        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('GET', '/api/titles');

        $response->assertStatus(200);
    }
}

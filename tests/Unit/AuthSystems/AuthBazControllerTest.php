<?php

namespace Tests\Unit\AuthSystems;

use Tests\TestCase;

class AuthBazControllerTest extends TestCase
{
    /**
     * @dataProvider getCredentials
     */
    public function testShoudLogin(array $credentials)
    {
        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/api/login', $credentials);

        $response->assertStatus(200);
    }

    /**
     * @dataProvider getInvalidCredentials
     */
    public function testShouldntLogin($credentials)
    {
        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('POST', '/api/login', $credentials);

        $response->assertStatus(401);
    }

    public static function getCredentials()
    {
        return [
            [
                ['login' => 'BAZ_1', 'password' => 'foo-bar-baz']
            ]
        ];
    }

    public static function getInvalidCredentials()
    {
        return [
            [
                ['login' => 'Baz_1', 'password' => 'foo-bar-baz']
            ]
        ];
    }
}

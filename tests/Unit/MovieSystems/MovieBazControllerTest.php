<?php

namespace Tests\Unit\MovieSystems;

use Tests\TestCase;

class MovieBazControllerTest extends TestCase
{
    public function testShoudListTitles() {
        $response = $this->withHeaders([
            'X-Header' => 'Value',
        ])->json('GET', '/api/titles');

        $response->assertStatus(200);
    }
}
